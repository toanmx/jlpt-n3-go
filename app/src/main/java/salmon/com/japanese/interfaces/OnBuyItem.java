package salmon.com.japanese.interfaces;

import salmon.com.japanese.models.JLPT;

/**
 * Created by Administrator on 8/1/2016.
 */
public interface OnBuyItem {
    void onBuyItem(JLPT jlpt);
}
