package salmon.com.japanese.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import salmon.com.japanese.R;
import salmon.com.japanese.adapters.QuestionJPLTApdapter;
import salmon.com.japanese.database.UserDatabase;
import salmon.com.japanese.models.Question;

public class TestActivity extends AppCompatActivity {
    FloatingActionButton fab;
    RecyclerView recyclerView;
    List<Question> questionList= new ArrayList<>();
    private UserDatabase db;
    private ProgressBar pbLoadQuestion;
    CountDownTimer countDownTimer;
    SeekBar seekBar;
    private static final String FORMAT = "%02d:%02d";
    boolean isPractice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        seekBar = (SeekBar) findViewById(R.id.seekbar);

        if(toolbar!=null){
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Title");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



        toolbar.collapseActionView();


        recyclerView= (RecyclerView) findViewById(R.id.rv_question);
        pbLoadQuestion = (ProgressBar) findViewById(R.id.pbLoadQuestion);
        SetListQuestionAsynctask setListQuestionAsynctask = new SetListQuestionAsynctask();
        setListQuestionAsynctask.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override  public void onBackPressed() {
        showBackTestDialog();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_done:
                int score = 0;
                int emptyAnswer=0;
                for (int i =0;i<questionList.size();i++){
                    if (questionList.get(i).getUser_answer() == questionList.get(i).getRight_answer())
                        score++;
                    else if  (questionList.get(i).user_answer ==0){
                        emptyAnswer++;
                    }
                }
                if (emptyAnswer>=20){
                    showCancelTestDialog(score);
                }
                else {
                    Intent intent = new Intent(TestActivity.this, ViewResultActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("finalScore",score);
                    startActivity(intent);
                }
                break;
            default:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    private void showBackTestDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(TestActivity.this);


        builder.setMessage("Do you want to end this test?\nYour progress will not be saved.\n");
        String positiveText = "Yes";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       finish();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
    private void showCancelTestDialog(final int score){
        AlertDialog.Builder builder = new AlertDialog.Builder(TestActivity.this);
        builder.setTitle("Oop!!!");

        builder.setMessage("Are you sure you want finish test?\nYou don't answer a lot of question!\n");
        String positiveText = "Yes";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(TestActivity.this, ViewResultActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("finalScore",score);
                        startActivity(intent);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
    private void showTimeOutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(TestActivity.this);
        builder.setTitle("Time out");
        builder.setIcon(R.drawable.chronometer_outline);
        builder.setMessage("Do you want need more time?");
        String positiveText = "Yes";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        String negativeText = "No, check results";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
    public class SetListQuestionAsynctask extends AsyncTask<Integer , Integer, List<Question>> {

        @Override
        protected List<Question> doInBackground(Integer... params) {
            db = new UserDatabase(getBaseContext());
            List<Question> questions;
            int idJLPT = getIntent().getIntExtra("idJLPT",0);
            if (idJLPT!=0){
                questions = db.getListQuestion(idJLPT);
                isPractice =false;
                return questions;
            }
            isPractice =true;
            questions = db.getListPracticeQuestion(getIntent().getIntExtra("idPractice",0));
            db.close();
            return questions;

        }
        @Override
        protected void onPostExecute(List<Question> question) {
            super.onPostExecute(question);

            questionList = question;

            QuestionJPLTApdapter questionJPLTApdapter = new QuestionJPLTApdapter(questionList, getBaseContext());
            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager1);
            recyclerView.setAdapter(questionJPLTApdapter);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setVisibility(View.VISIBLE);
            pbLoadQuestion.setVisibility(View.GONE);
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
            if (isPractice) {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "", 4000)
                        .setAction("Action", null);
                View snackbarView = snackbar.getView();
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setMaxLines(5);
                switch (getIntent().getIntExtra("idPractice", 0)) {
                    case 1:
                        textView.setText("問題１　＿＿＿のことばの読み方として最もよいものを、１・２・３・４から一つえらびなさい。");
                        break;
                    case 2:
                        textView.setText("問題２　＿＿＿のことばを漢字で書くとき、最もよいものを、１・２・３・４から一つえらびなさい。 ");
                        break;
                    case 3:
                        textView.setText("問題3　（　　　）に入れるのに最もよいものを、１・２・３・４から一つえらびなさい。");
                        break;
                    case 4:
                        textView.setText("問題4　＿＿＿に意味が最も近いものを、１・２・３・４から一つえらびなさい。");
                        break;
                    case 5:
                        textView.setText("問題5　つぎのことばの使い方として最もよいものを、１・２・３・４から一つえらびなさい。");
                        break;

                }

                snackbar.show();
            }
        }
    }
}
