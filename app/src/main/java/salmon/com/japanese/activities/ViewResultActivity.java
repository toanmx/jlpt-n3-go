package salmon.com.japanese.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;


import salmon.com.japanese.R;

/**
 * Created by Administrator on 8/9/2016.
 */
public class ViewResultActivity extends AppCompatActivity {
    TextView tvScore,tvCongratulation,tvReviewPoint,tvReceiveFlash;
    ImageView ivEmoji;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_finish_test);
        getSupportActionBar().hide();
        tvScore = (TextView) findViewById(R.id.tvScore);
        tvCongratulation = (TextView) findViewById(R.id.tvCongratulation);
        tvReviewPoint = (TextView) findViewById(R.id.tvReviewPoint);
        tvReceiveFlash = (TextView) findViewById(R.id.tvReceiveFlash);
        int score = getIntent().getIntExtra("finalScore",0);
        tvScore.setText(score+"/35");
        Window window = getWindow();
        ivEmoji = (ImageView) findViewById(R.id.ivEmoji);
        SharedPreferences sharedpreferences = getSharedPreferences("user_profile", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("user_profile", true);
        int testQuantity= sharedpreferences.getInt("tests_quantity",0);
        float jlptAvarage = sharedpreferences.getFloat("jlpt_avarage",0);

        editor.putInt("tests_quantity", testQuantity+1);
        editor.putFloat("jlpt_avarage", (jlptAvarage+ score)/(testQuantity+1));


        if (score <10 ){
            ivEmoji.setImageDrawable(getResources().getDrawable(R.drawable.emoji_5));
            tvCongratulation.setText("すみません。");
            tvReceiveFlash.setText("You don't receive flashs");
            tvReviewPoint.setText("Oops, try again!");
        }
        else if (score>=10&&score<15){
            ivEmoji.setImageDrawable(getResources().getDrawable(R.drawable.emoji_4));
            tvCongratulation.setText("すみません。");
            tvReviewPoint.setText("Try again!");
        }
        else if (score>=15&&score<25){
            editor.putInt("user_flashs", sharedpreferences.getInt("user_flashs",0)+2);
            ivEmoji.setImageDrawable(getResources().getDrawable(R.drawable.emoji_3));
            tvCongratulation.setText("おめでとう。");
            tvReceiveFlash.setText(Html.fromHtml("You receive <b>2</b> flashs"));
            tvReviewPoint.setText("Good job");
        }
        else if (score>=25&&score<35){
            editor.putInt("user_flashs", sharedpreferences.getInt("user_flashs",0)+3);
            ivEmoji.setImageDrawable(getResources().getDrawable(R.drawable.emoji_1));
            tvCongratulation.setText("おめでとう。");
            tvReceiveFlash.setText(Html.fromHtml("You receive <b>3</b> flashs"));
            tvReviewPoint.setText("Excellent");
        }
        else {
            editor.putInt("user_flashs", sharedpreferences.getInt("user_flashs",0)+5);
            ivEmoji.setImageDrawable(getResources().getDrawable(R.drawable.ic_congratulation));
            tvCongratulation.setText("おめでとう。");
            tvReceiveFlash.setText(Html.fromHtml("You receive <b>5</b> flashs"));
            tvReviewPoint.setText("Perfect!");
        }

        editor.commit();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.parseColor("#5075AC"));
        }

    }

    @Override
    public void onBackPressed() {

    }

    public void OnClickDoneText(View view){
        Intent intent = new Intent(ViewResultActivity.this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void OnClickViewFailDetails(View view){

    }
    public void OnClickArrowDown(View view){

    }
    public void OnClickShareOnFacebook(View view){

    }
    public void OnClickShareOnTwitter(View view){

    }
}
