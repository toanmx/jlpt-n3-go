package salmon.com.japanese.activities;

import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import java.util.ArrayList;
import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.fragments.EventFragment;
import salmon.com.japanese.fragments.ProfileFragment;
import salmon.com.japanese.fragments.SchoolFragment;
import salmon.com.japanese.fragments.SettingFragment;
import salmon.com.japanese.interfaces.OnBuyItem;
import salmon.com.japanese.models.JLPT;

public class MainActivity extends AppCompatActivity implements OnBuyItem {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ViewPagerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.profile_viewpager);
        setupViewPager(viewPager,savedInstanceState);

        tabLayout = (TabLayout) findViewById(R.id.profile_tab);
        getSupportActionBar().hide();

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_school_white);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_whatshot_white);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_person_white);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_settings_white);
        getFragmentManager();
    }
    private void setupViewPager(ViewPager viewPager,Bundle savedInstanceState) {
        adapter = new ViewPagerAdapter(MainActivity.this.getSupportFragmentManager());
        if (savedInstanceState!=null){

            SchoolFragment schoolFragment = (SchoolFragment) getSupportFragmentManager().getFragment(savedInstanceState,"schoolfragment");
            adapter.addFragment(schoolFragment, "");

            adapter.addFragment(new EventFragment(), "");
            adapter.addFragment(new ProfileFragment(), "");
            adapter.addFragment(new SettingFragment(), "");
        }
        else{
            adapter.addFragment(new SchoolFragment(), "");
            adapter.addFragment(new EventFragment(), "");
            adapter.addFragment(new ProfileFragment(), "");
            adapter.addFragment(new SettingFragment(), "");
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    @Override
    public void onBuyItem(JLPT jlpt) {


        SchoolFragment schoolFragment = (SchoolFragment) adapter.getFirstFragment();
        if (schoolFragment!=null){
            schoolFragment.updateBuyItem(jlpt);
        }
        else {

        }


    }

    //new work 1
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public Fragment getFirstFragment(){
            return mFragmentList.get(0);
        }
        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        // change scrên
        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}