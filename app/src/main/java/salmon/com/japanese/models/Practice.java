package salmon.com.japanese.models;

/**
 * Created by Administrator on 7/24/2016.
 */
public class Practice {
    private String title;
    private int flashQuantity;

    public Practice(String title, int flashQuantity) {
        this.title = title;
        this.flashQuantity = flashQuantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFlashQuantity() {
        return flashQuantity;
    }

    public void setFlashQuantity(int flashQuantity) {
        this.flashQuantity = flashQuantity;
    }
}
