package salmon.com.japanese.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 7/24/2016.
 */
public class JLPT implements Parcelable{
    private int id;
    private String title;
    private int flashQuantity;
    private int testStatus;
    private int buyStatus;
    private int flashCost;
    private float hightestPoint;
    public JLPT(int id, String title, int testStatus, int buyStatus, int flashQuantity, int flashCost, float hightestPoint) {
        this.id = id;
        this.title = title;
        this.flashQuantity = flashQuantity;
        this.testStatus = testStatus;
        this.buyStatus = buyStatus;
        this.flashCost = flashCost;
        this.hightestPoint = hightestPoint;
    }

    protected JLPT(Parcel in) {
        id = in.readInt();
        title = in.readString();
        flashQuantity = in.readInt();
        testStatus = in.readInt();
        buyStatus = in.readInt();
        flashCost = in.readInt();
        hightestPoint = in.readFloat();
    }

    public static final Creator<JLPT> CREATOR = new Creator<JLPT>() {
        @Override
        public JLPT createFromParcel(Parcel in) {
            return new JLPT(in);
        }

        @Override
        public JLPT[] newArray(int size) {
            return new JLPT[size];
        }
    };

    public int getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(int testStatus) {
        this.testStatus = testStatus;
    }

    public int getBuyStatus() {
        return buyStatus;
    }

    public void setBuyStatus(int buyStatus) {
        this.buyStatus = buyStatus;
    }

    public JLPT(int id, String title, int flashQuantity, int status, int flashCost, float hightestPoint) {
        this.id = id;
        this.title = title;
        this.flashQuantity = flashQuantity;
        this.testStatus = status;
        this.flashCost = flashCost;
        this.hightestPoint = hightestPoint;
    }

    public JLPT(int id, String title, int flashQuantity, int status) {
        this.id = id;
        this.title = title;
        this.flashQuantity = flashQuantity;
        this.testStatus = status;
    }

    public int getFlashCost() {
        return flashCost;
    }

    public void setFlashCost(int flashCost) {
        this.flashCost = flashCost;
    }

    public float getHightestPoint() {
        return hightestPoint;
    }

    public void setHightestPoint(float hightestPoint) {
        this.hightestPoint = hightestPoint;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFlashQuantity() {
        return flashQuantity;
    }

    public void setFlashQuantity(int flashQuantity) {
        this.flashQuantity = flashQuantity;
    }

    public int isStatus() {
        return testStatus;
    }

    public void setStatus(int status) {
        this.testStatus = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeInt(flashQuantity);
        dest.writeInt(testStatus);
        dest.writeInt(buyStatus);
        dest.writeInt(flashCost);
        dest.writeFloat(hightestPoint);
    }
}
