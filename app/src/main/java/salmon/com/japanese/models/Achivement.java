package salmon.com.japanese.models;

/**
 * Created by Administrator on 8/9/2016.
 */
public class Achivement {
    private String title;
    private int flashReceive;
    private boolean available;

    public Achivement(String title, int flashReceive, boolean available) {
        this.title = title;
        this.flashReceive = flashReceive;
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFlashReceive() {
        return flashReceive;
    }

    public void setFlashReceive(int flashReceive) {
        this.flashReceive = flashReceive;
    }
}
