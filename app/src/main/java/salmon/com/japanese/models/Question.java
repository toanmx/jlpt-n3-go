package salmon.com.japanese.models;

/**
 * Created by 8470p on 6/30/2016.
 */
public class Question {
    private int id;
    private String question;
    private String answer_a;
    private String answer_b;
    private String answer_c;
    private String answer_d;
    private int right_answer;
    public int user_answer = NONE; // hold the answer picked by the user, initial is NONE(see below)
    public static final int NONE = 0; // No answer selected
    public static final int ANSWER_ONE_SELECTED = 1; // first answer selected
    public static final int ANSWER_TWO_SELECTED = 2; // second answer selected
    public static final int ANSWER_THREE_SELECTED = 3; // third answer selected
    public static final int ANSWER_FOUR_SELECTED = 4; // forth answer selected

    public Question(int id, String question, String answer_a, String answer_b, String answer_c, String answer_d, int right_answer, int user_answer) {
        this.id = id;
        this.question = question;
        this.answer_a = answer_a;
        this.answer_b = answer_b;
        this.answer_c = answer_c;
        this.answer_d = answer_d;
        this.right_answer = right_answer;
        this.user_answer = user_answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer_a() {
        return answer_a;
    }

    public void setAnswer_a(String answer_a) {
        this.answer_a = answer_a;
    }

    public String getAnswer_b() {
        return answer_b;
    }

    public void setAnswer_b(String answer_b) {
        this.answer_b = answer_b;
    }

    public String getAnswer_c() {
        return answer_c;
    }

    public void setAnswer_c(String answer_c) {
        this.answer_c = answer_c;
    }

    public String getAnswer_d() {
        return answer_d;
    }

    public void setAnswer_d(String answer_d) {
        this.answer_d = answer_d;
    }

    public int getRight_answer() {
        return right_answer;
    }

    public void setRight_answer(int right_answer) {
        this.right_answer = right_answer;
    }

    public int getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(int user_answer) {
        this.user_answer = user_answer;
    }
}
