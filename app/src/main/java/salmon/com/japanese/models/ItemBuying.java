package salmon.com.japanese.models;

/**
 * Created by Admin on 7/13/2016.
 */
public class ItemBuying {
    private int idItemBuying;
    private String idTitleItemBuying;
    private int valueItemBuying;

    public ItemBuying(int idItemBuying, String idTitleItemBuying, int valueItemBuying) {
        this.idItemBuying = idItemBuying;
        this.idTitleItemBuying = idTitleItemBuying;
        this.valueItemBuying = valueItemBuying;
    }

    public int getIdItemBuying() {
        return idItemBuying;
    }

    public void setIdItemBuying(int idItemBuying) {
        this.idItemBuying = idItemBuying;
    }

    public String getIdTitleItemBuying() {
        return idTitleItemBuying;
    }

    public void setIdTitleItemBuying(String idTitleItemBuying) {
        this.idTitleItemBuying = idTitleItemBuying;
    }

    public int getValueItemBuying() {
        return valueItemBuying;
    }

    public void setValueItemBuying(int valueItemBuying) {
        this.valueItemBuying = valueItemBuying;
    }
}
