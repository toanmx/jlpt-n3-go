package salmon.com.japanese.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.activities.TestActivity;
import salmon.com.japanese.adapters.JLPTAdapter;
import salmon.com.japanese.adapters.JLPTStoreAdapter;
import salmon.com.japanese.database.UserDatabase;
import salmon.com.japanese.interfaces.OnBuyItem;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.JLPT;


/**
 * Created by 8470p on 6/21/2016.
 */
public class EventFragment extends android.support.v4.app.Fragment {
    private TextView tvUserFlash;
    private Context mContext;
    private int userFlash;
    RecyclerView jlptRecycleView;
    List<JLPT> jlpts = new ArrayList<>();
    JLPTStoreAdapter jlptAdapter;
    SharedPreferences sharedpreferences;
    UserDatabase db;
    OnBuyItem mCallback;
    OpenDatabaseAsynctask openDatabaseAsynctask;
    RelativeLayout storeData;
    ProgressBar pbLoadData;
    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnBuyItem) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBuyItem");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event, container, false);


        jlptRecycleView = (RecyclerView) rootView.findViewById(R.id.rv_jlpt_store);
        tvUserFlash = (TextView) rootView.findViewById(R.id.tv_user_flash);
        storeData = (RelativeLayout) rootView.findViewById(R.id.storeData);
        pbLoadData = (ProgressBar) rootView.findViewById(R.id.pbLoadStoreData);
        jlptRecycleView.setFocusable(false);
        sharedpreferences = mContext.getSharedPreferences("user_profile",Context.MODE_PRIVATE);

        if (sharedpreferences.getBoolean("user_profile",false)) {
            userFlash = sharedpreferences.getInt("user_flashs", 0);
            tvUserFlash.setText(userFlash+"");
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null){
            jlpts = savedInstanceState.getParcelableArrayList("storeData");

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            jlptAdapter = new JLPTStoreAdapter(EventFragment.this,jlpts);

            jlptRecycleView.setLayoutManager(layoutManager);
            jlptRecycleView.setAdapter(jlptAdapter);

            jlptRecycleView.setItemAnimator(new DefaultItemAnimator());
            storeData.setVisibility(View.VISIBLE);
            pbLoadData.setVisibility(View.GONE);
        }
        else {
            openDatabaseAsynctask = new OpenDatabaseAsynctask();
            openDatabaseAsynctask.execute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("storeData", (ArrayList<? extends Parcelable>) jlpts);

    }

    public void onColorsListItemClicked(View view) {
        int itemAdapterPosition = jlptRecycleView.getChildAdapterPosition(view);

        if (userFlash<jlpts.get(itemAdapterPosition).getFlashCost()){
            Toast.makeText(mContext,"You don't have enough flash",Toast.LENGTH_SHORT).show();
        }
        else {
            mCallback.onBuyItem(jlpts.get(itemAdapterPosition));
            db.updateBoughtJLPTItem(jlpts.get(itemAdapterPosition).getId());
            userFlash = userFlash - jlpts.get(itemAdapterPosition).getFlashCost();
            tvUserFlash.setText(userFlash+"");
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt("user_flashs",userFlash);
            editor.commit();
            jlptAdapter.removeItemAtPosition(itemAdapterPosition);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
        openDatabaseAsynctask.cancel(true);
    }

    @Override
    public void onStop() {
        super.onStop();

    }
    public class OpenDatabaseAsynctask extends AsyncTask<String , Integer, List<JLPT>> {
        @Override
        protected List<JLPT> doInBackground(String... params) {
            db = new UserDatabase(getContext());
            jlpts = db.getListJLPT(0);
            return jlpts;
        }
        @Override
        protected void onPostExecute(List<JLPT> result) {
            super.onPostExecute(result);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            layoutManager.setSmoothScrollbarEnabled(false);
            jlptAdapter = new JLPTStoreAdapter(EventFragment.this,result);

            jlptRecycleView.setLayoutManager(layoutManager);
            jlptRecycleView.setAdapter(jlptAdapter);

            jlptRecycleView.setItemAnimator(new DefaultItemAnimator());
            storeData.setVisibility(View.VISIBLE);
            pbLoadData.setVisibility(View.GONE);
        }
    }
}
