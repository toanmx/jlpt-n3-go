package salmon.com.japanese.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.activities.TestActivity;
import salmon.com.japanese.adapters.JLPTAdapter;
import salmon.com.japanese.adapters.PracticeAdapter;
import salmon.com.japanese.database.UserDatabase;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.JLPT;
import salmon.com.japanese.models.Practice;


/**
 * Created by 8470p on 6/21/2016.
 */
public class SchoolFragment extends android.support.v4.app.Fragment implements View.OnClickListener {
    private Button bt_gotit;
    private  RelativeLayout rl_welcome;
    List<JLPT> jlptList = new ArrayList<>();
    JLPTAdapter jlptAdapter;
    UserDatabase db;
    PracticeAdapter practiceAdapter ;
    RecyclerView jlptRecycleView;
    RecyclerView practiceRecycleView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_school, container, false);

        initView(rootView);

        bt_gotit.setOnClickListener(this);

        practiceRecycleView = (RecyclerView) rootView.findViewById(R.id.rv_practice);
        jlptRecycleView = (RecyclerView) rootView.findViewById(R.id.rv_jlpt_test);

        SetListPracticeAsynctask setListPracticeAsynctask = new SetListPracticeAsynctask();
        setListPracticeAsynctask.execute();


        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void initView(View view){
        bt_gotit = (Button) view.findViewById(R.id.bt_got_it);
        rl_welcome = (RelativeLayout) view.findViewById(R.id.rl_welcome);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.bt_got_it:
                rl_welcome.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("jlptData", (ArrayList<? extends Parcelable>) jlptList);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            jlptList = savedInstanceState.getParcelableArrayList("jlptData");
            setContent();
        }
        else {
            OpenDatabaseAsynctask openDatabaseAsynctask = new OpenDatabaseAsynctask();
            openDatabaseAsynctask.execute();
        }
    }

    public void updateBuyItem(JLPT jlpt){
        jlptList.add(jlpt);
        jlptAdapter.notifyDataSetChanged();
    }
    public class OpenDatabaseAsynctask extends AsyncTask<String , Integer, UserDatabase> {
        @Override
        protected UserDatabase doInBackground(String... params) {
            UserDatabase db = new UserDatabase(getContext());
            jlptList = db.getListJLPT(1);
            return db;
        }
        @Override
        protected void onPostExecute(UserDatabase userDatabase) {
            super.onPostExecute(userDatabase);
            db = userDatabase;
            setContent();
        }
    }
    private void setContent(){
        jlptAdapter = new JLPTAdapter(jlptList,getContext());

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        jlptRecycleView.setLayoutManager(linearLayoutManager1);
        jlptRecycleView.setAdapter(jlptAdapter);

        jlptAdapter.SetOnItemClickListener(new RecycleViewOnItemClick() {
            @Override
            public void SetOnItemClickListener(View v, int positon) {
                Intent intent =new Intent(getContext(),TestActivity.class) ;
                intent.putExtra("idJLPT",jlptList.get(positon).getId());
                startActivity(intent);
            }
        });
    }
    public class SetListPracticeAsynctask extends AsyncTask<String , Integer, PracticeAdapter> {

        @Override
        protected PracticeAdapter doInBackground(String... params) {
            List<Practice> practiceList = new ArrayList<>();

            practiceList.add(new Practice("あ",1));
            practiceList.add(new Practice("い",2));
            practiceList.add(new Practice("う",3));
            practiceList.add(new Practice("え",3));
            practiceList.add(new Practice("お",2));
            PracticeAdapter practice = new PracticeAdapter(practiceList,getContext());

            return practice;
        }
        @Override
        protected void onPostExecute(PracticeAdapter practice) {
            super.onPostExecute(practice);
            practiceAdapter = practice;
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
            practiceRecycleView.setLayoutManager(linearLayoutManager);
            practiceRecycleView.setAdapter(practiceAdapter);
            practiceAdapter.SetOnItemClickListener(new RecycleViewOnItemClick() {
                @Override
                public void SetOnItemClickListener(View v, int positon) {
                    Intent intent = new Intent(getContext(),TestActivity.class);
                    intent.putExtra("idPractice",(positon+1));

                    startActivity(intent);

                }
            });
        }
    }

}
