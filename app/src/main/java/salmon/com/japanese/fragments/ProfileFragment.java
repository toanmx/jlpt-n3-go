package salmon.com.japanese.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.adapters.AchivementAdapter;
import salmon.com.japanese.adapters.JLPTStoreAdapter;
import salmon.com.japanese.models.Achivement;


/**
 * Created by 8470p on 6/21/2016.
 */
public class ProfileFragment extends android.support.v4.app.Fragment {
    private Context mContext;
    private AchivementAdapter achivementAdapter;
    private List<Achivement> achivementList = new ArrayList<>();
    private RecyclerView rvAchivement;

    ScrollView scrollView;
    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        SharedPreferences sharedpreferences = mContext.getSharedPreferences("user_profile",Context.MODE_PRIVATE);
        rvAchivement = (RecyclerView) rootView.findViewById(R.id.rvAchivement);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollViewProfile);
        scrollView.fullScroll(View.FOCUS_UP);


        achivementList.add(new Achivement("Finished ",5,true));
        achivementList.add(new Achivement("Finished ",2,false));
        achivementList.add(new Achivement("Finished ",2,false));

        LinearLayoutManager linearLayoutManager ;
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false) ;
        rvAchivement.setFocusable(false);
        achivementAdapter = new AchivementAdapter(ProfileFragment.this,achivementList);

        rvAchivement.setLayoutManager(linearLayoutManager);
        rvAchivement.setAdapter(achivementAdapter);

        rvAchivement.setItemAnimator(new DefaultItemAnimator());

        if (sharedpreferences.getBoolean("user_profile",false)){
            ((TextView) rootView.findViewById(R.id.tests_quantity)).setText(sharedpreferences.getInt("tests_quantity",0)+"");
            ((TextView) rootView.findViewById(R.id.practices_quantity)).setText(sharedpreferences.getInt("practices_quantity",0)+"");
            ((TextView) rootView.findViewById(R.id.jlpt_average)).setText(new DecimalFormat("##.##").format(sharedpreferences.getFloat("jlpt_avarage",0))+"/35");
        }
        return rootView;
    }
    public void onListAchivementClicked(View view) {
        int itemAdapterPosition = rvAchivement.getChildAdapterPosition(view);
        if (achivementList.get(itemAdapterPosition).isAvailable()){
            achivementAdapter.onListAchivementClicked(itemAdapterPosition);
        }
    }
}
