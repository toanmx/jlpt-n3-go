package salmon.com.japanese.fragments;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;

import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.models.Question;

/**
 * Created by Admin on 10/4/2016.
 */
public class ViewFailDetailsFragment extends BottomSheetDialogFragment {
    private List<Question> questionList;

    public ViewFailDetailsFragment(List<Question> questions){
        this.questionList = questions;
    }
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_view_fail_details, null);
        dialog.setContentView(contentView);
    }
}
