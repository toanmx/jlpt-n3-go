package salmon.com.japanese.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.models.Question;

/**
 * Created by 8470p on 6/30/2016.
 */
public class QuestionAdapter extends BaseAdapter {
    List<Question> data;
    Context mContext;
    int mPosition;
    int ans = 0;
    public QuestionAdapter(List<Question> data, Context mContext){
        super();
        this.data = data;
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder myHolder = new Holder();
        if (convertView == null){

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_question,parent,false);
            myHolder.question = (TextView) convertView.findViewById(R.id.item_tv_question);
            myHolder.groupAnswer = (RadioGroup) convertView.findViewById(R.id.rd_group_answer);
            myHolder.answer_a = (RadioButton) convertView.findViewById(R.id.rb_item_answer_a);
            myHolder.answer_b = (RadioButton) convertView.findViewById(R.id.rb_item_answer_b);
            myHolder.answer_c = (RadioButton) convertView.findViewById(R.id.rb_item_answer_c);
            myHolder.answer_d = (RadioButton) convertView.findViewById(R.id.rb_item_answer_d);
            convertView.setTag(myHolder);

        }
        else {
            myHolder = (Holder) convertView.getTag();
        }

        final Question element = data.get(position);
        myHolder.question.setText(Html.fromHtml(element.getQuestion()));
        myHolder.answer_a.setText(element.getAnswer_a());
        myHolder.answer_b.setText(element.getAnswer_b());
        myHolder.answer_c.setText(element.getAnswer_c());
        myHolder.answer_d.setText(element.getAnswer_d());
        myHolder.groupAnswer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) { //set the Model to hold the answer the user picked

                    case R.id.rb_item_answer_a:
                        element.user_answer = Question.ANSWER_ONE_SELECTED;
                        notifyDataSetChanged();
                        break;
                    case R.id.rb_item_answer_b:
                        element.user_answer = Question.ANSWER_TWO_SELECTED;
                        notifyDataSetChanged();
                        break;
                    case R.id.rb_item_answer_c:
                        element.user_answer = Question.ANSWER_THREE_SELECTED;
                        notifyDataSetChanged();
                        break;
                    case R.id.rb_item_answer_d:
                        element.user_answer = Question.ANSWER_FOUR_SELECTED;
                        notifyDataSetChanged();
                        break;
                    default:
                        element.user_answer = Question.NONE; // Something was wrong set to the default
                }

            }
        });

        if (data.get(position).user_answer != Question.NONE) {
            RadioButton r = (RadioButton) myHolder.groupAnswer.getChildAt(data
                    .get(position).user_answer);
            r.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            for (int i=0;i<4;i++)
                if (i!=data.get(position).user_answer)
                    ((RadioButton) myHolder.groupAnswer.getChildAt(i)).setTextColor(mContext.getResources().getColor(R.color.colorTextRadioButton));
            r.setChecked(true);
        } else {
            myHolder.groupAnswer.clearCheck();
            for (int i=0;i<4;i++)
                ((RadioButton) myHolder.groupAnswer.getChildAt(i)).setTextColor(mContext.getResources().getColor(R.color.colorTextRadioButton));
            // This is required because although the Model could have the current
            // position to NONE you could be dealing with a previous row where
            // the user already picked an answer.
        }
        return convertView;
    }

    public class Holder{
        public TextView question;
        public RadioGroup groupAnswer;
        public RadioButton answer_a;
        public RadioButton answer_b;
        public RadioButton answer_c;
        public RadioButton answer_d;
    }
}