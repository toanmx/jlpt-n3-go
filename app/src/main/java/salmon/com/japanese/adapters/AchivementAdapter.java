package salmon.com.japanese.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import salmon.com.japanese.R;
import salmon.com.japanese.fragments.EventFragment;
import salmon.com.japanese.fragments.ProfileFragment;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.Achivement;
import salmon.com.japanese.models.JLPT;

/**
 * Created by Administrator on 8/9/2016.
 */
public class AchivementAdapter extends RecyclerView.Adapter<AchivementAdapter.JLPTStoreViewHolder> {

    private ProfileFragment profileFragment;
    private List<Achivement> achivementList = new ArrayList<>();

    public AchivementAdapter(ProfileFragment eventFragment,List<Achivement> achivementList) {
        this.profileFragment = eventFragment;
        this.achivementList = achivementList;
    }

    @Override
    public JLPTStoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_achivement, parent, false);

        JLPTStoreViewHolder colorViewHolder = new JLPTStoreViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileFragment.onListAchivementClicked(v);
            }
        });
        return colorViewHolder;
    }

    @Override
    public void onBindViewHolder(JLPTStoreViewHolder holder, int position) {
        holder.tvTitle.setText(achivementList.get(position).getTitle());
        if (achivementList.get(position).isAvailable()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.ll.setBackground(profileFragment.getResources().getDrawable(R.drawable.click_test_drawer));

            }
            holder.iconGift.setImageDrawable(profileFragment.getResources().getDrawable(R.drawable.gift));
        }
        else {
            holder.iconGift.setImageDrawable(profileFragment.getResources().getDrawable(R.drawable.pencil2));
        }
    }

    @Override
    public int getItemCount() {
        return achivementList.size();
    }

    public void onListAchivementClicked(int position) {
        achivementList.remove(position);
        notifyItemRemoved(position);

    }

    static class JLPTStoreViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_title_achivement)
        TextView tvTitle;
        @Bind(R.id.item_linearlayout)
        LinearLayout ll;
        @Bind(R.id.icon_gift)
        ImageView iconGift;
        public JLPTStoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}