package salmon.com.japanese.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.Question;

/**
 * Created by Administrator on 7/25/2016.
 */
public class QuestionJPLTApdapter extends RecyclerView.Adapter<QuestionJPLTApdapter.MyViewHolder> {
    private List<Question> questionList;
    private Context context;
    RecycleViewOnItemClick recycleViewOnItemClick;

    public QuestionJPLTApdapter(List<Question> questionList, Context context) {
        this.questionList = questionList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder myHolder, final int position) {
        myHolder.tv_question.setText(Html.fromHtml(questionList.get(position).getQuestion()));
        myHolder.answer_a.setText(Html.fromHtml(questionList.get(position).getAnswer_a()));
        myHolder.answer_b.setText(Html.fromHtml(questionList.get(position).getAnswer_b()));
        myHolder.answer_c.setText(Html.fromHtml(questionList.get(position).getAnswer_c()));
        myHolder.answer_d.setText(Html.fromHtml(questionList.get(position).getAnswer_d()));
        myHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) { //set the Model to hold the answer the user picked
                    case R.id.rb_item_answer_a:
                        questionList.get(position).user_answer = Question.ANSWER_ONE_SELECTED;
                        break;
                    case R.id.rb_item_answer_b:
                        questionList.get(position).user_answer = Question.ANSWER_TWO_SELECTED;
                        break;
                    case R.id.rb_item_answer_c:
                        questionList.get(position).user_answer = Question.ANSWER_THREE_SELECTED;
                        break;
                    case R.id.rb_item_answer_d:
                        questionList.get(position).user_answer = Question.ANSWER_FOUR_SELECTED;
                        break;
                    default:
                        questionList.get(position).user_answer = Question.NONE; // Something was wrong set to the default
                }
            }
        });
        if (questionList.get(position).user_answer !=Question.NONE){
            RadioButton r = (RadioButton) myHolder.radioGroup.getChildAt(questionList
                    .get(position).user_answer-1);
            ;
        }
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements RadioGroup.OnCheckedChangeListener {
        TextView tv_question;
        public RadioButton answer_a;
        public RadioButton answer_b;
        public RadioButton answer_c;
        public RadioButton answer_d;
        public RadioGroup radioGroup;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_question = (TextView) itemView.findViewById(R.id.item_tv_question);
            answer_a = (RadioButton) itemView.findViewById(R.id.rb_item_answer_a);
            answer_b = (RadioButton) itemView.findViewById(R.id.rb_item_answer_b);
            answer_c = (RadioButton) itemView.findViewById(R.id.rb_item_answer_c);
            answer_d = (RadioButton) itemView.findViewById(R.id.rb_item_answer_d);
            radioGroup = (RadioGroup) itemView.findViewById(R.id.rd_group_answer);
        }



        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

        }

    }
}
