package salmon.com.japanese.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.Practice;

/**
 * Created by Administrator on 7/24/2016.
 */
public class PracticeAdapter  extends RecyclerView.Adapter<PracticeAdapter.MyViewHolder>{
    private List<Practice> practiceList;
    private Context context;
    RecycleViewOnItemClick recycleViewOnItemClick;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tv_title;
        public TextView tv_flash_quantity;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title_item_practice);
            tv_flash_quantity = (TextView) itemView.findViewById(R.id.tv_flash_quantity_item_practice);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            recycleViewOnItemClick.SetOnItemClickListener(v,getPosition());
        }
    }
    public PracticeAdapter(List<Practice> practiceList,Context context){
        this.practiceList = practiceList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_practice,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"font/masugo.otf");
        holder.tv_title.setTypeface(typeface);
        holder.tv_title.setText(practiceList.get(position).getTitle());
        holder.tv_flash_quantity.setText("Get "+practiceList.get(position).getFlashQuantity()+" flashes!");
    }

    @Override
    public int getItemCount() {
        return practiceList.size();
    }
    public void SetOnItemClickListener(final RecycleViewOnItemClick mItemClickListener) {
        this.recycleViewOnItemClick = mItemClickListener;
    }
}
