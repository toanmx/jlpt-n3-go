package salmon.com.japanese.adapters;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import salmon.com.japanese.R;
import salmon.com.japanese.fragments.EventFragment;
import salmon.com.japanese.models.JLPT;

/**public JLPTStoreAdapter(FragmentActivity , List<JLPT> ) {
    }
 * Created by Administrator on 7/26/2016.
 */
public class JLPTStoreAdapter extends RecyclerView.Adapter<JLPTStoreAdapter.JLPTStoreViewHolder> {

    private EventFragment eventFragment;
    private List<JLPT> jlpts = new ArrayList<>();

    public JLPTStoreAdapter(EventFragment eventFragment,List<JLPT> jlpts) {
        this.eventFragment = eventFragment;
        this.jlpts = jlpts;
    }

    @Override
    public JLPTStoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jlpt_store, parent, false);

        JLPTStoreViewHolder colorViewHolder = new JLPTStoreViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventFragment.onColorsListItemClicked(v);
            }
        });
        return colorViewHolder;
    }

    @Override
    public void onBindViewHolder(JLPTStoreViewHolder holder, int position) {
        holder.tvTitle.setText(jlpts.get(position).getTitle()+"\nGet now!");
        holder.tvFlashStore.setText(jlpts.get(position).getFlashCost()+"");
    }

    @Override
    public int getItemCount() {
        return jlpts.size();
    }

    public void removeItemAtPosition(int position) {
        jlpts.remove(position);
        notifyItemRemoved(position);

    }

    static class JLPTStoreViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_jlpt_store_title)
        TextView tvTitle;
        @Bind(R.id.tv_flash_store)
        TextView tvFlashStore;
        public JLPTStoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}