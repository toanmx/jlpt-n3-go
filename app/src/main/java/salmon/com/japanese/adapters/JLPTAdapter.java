package salmon.com.japanese.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import salmon.com.japanese.R;
import salmon.com.japanese.interfaces.RecycleViewOnItemClick;
import salmon.com.japanese.models.JLPT;
import salmon.com.japanese.models.Practice;
import salmon.com.japanese.models.Question;

/**
 * Created by Administrator on 7/24/2016.
 */
public class JLPTAdapter  extends RecyclerView.Adapter<JLPTAdapter.MyViewHolder>{
    private List<JLPT> jlptList;
    private Context context;
    RecycleViewOnItemClick recycleViewOnItemClick;
    private int lastPosition;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{
        public LinearLayout ll_container;
        public TextView tv_title;
        public TextView tv_flash_receive;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_jlpt_title);
            tv_flash_receive = (TextView) itemView.findViewById(R.id.tv_flash_receive);
            ll_container = (LinearLayout) itemView.findViewById(R.id.item_layout_container);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            recycleViewOnItemClick.SetOnItemClickListener(v,getPosition());
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

        }
    }
    public JLPTAdapter(List<JLPT> jlptList,Context context){
        this.jlptList = jlptList;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jlpt_test,parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv_title.setText(jlptList.get(position).getTitle()+"\n35 questions ");
        holder.tv_flash_receive.setText(jlptList.get(position).getFlashQuantity()+"");
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return jlptList.size();
    }
    public void SetOnItemClickListener(final RecycleViewOnItemClick mItemClickListener) {
        this.recycleViewOnItemClick = mItemClickListener;
    }

}
