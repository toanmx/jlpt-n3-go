package salmon.com.japanese.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import salmon.com.japanese.models.ItemBuying;

/**
 * Created by Admin on 7/13/2016.
 */
public class WhatshotAdapter extends BaseAdapter {
    private List<ItemBuying> itemBuyingList;
    private Context mContext;
    public WhatshotAdapter(List<ItemBuying> data, Context mContext){
        super();
        this.itemBuyingList = data;
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return itemBuyingList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
    public class Holder{
        TextView tv_title_item;
        TextView tv_value_item;
    }
}
