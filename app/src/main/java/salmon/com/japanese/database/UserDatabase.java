package salmon.com.japanese.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

import salmon.com.japanese.models.JLPT;
import salmon.com.japanese.models.Question;

/**
 * Created by 8470p on 6/27/2016.
 */
public class UserDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "JLPTExamManager";


    // Practices table name
    private static final String TABLE_PRACTICES = "practices";

    // Practice Table Columns names
    private static final String KEY_ID_PRACTICE = "idPractice";
    private static final String KEY_TITLE_PRACTICE = "titlePractice";
    private static final String KEY_STATUS_PRACTICE = "statusPractice";
    private static final String KEY_FLASH_QUANTITY_PRACTICE = "flashQuantityPractice";
    private static final String KEY_HIGHTEST_POINT_PRACTICE = "hightestPointPractice";

    //JLPT table name
    private static final String TABLE_JLPTS = "jlpts";

    //JLPT Table Columns names
    private static final String KEY_ID_JLPT = "idJLPT";
    private static final String KEY_TITLE_JLPT = "titleJLPT";
    private static final String KEY_TEST_STATUS = "testStatusJLPT";
    private static final String KEY_FLASH_QUANTITY_JLPT = "flashQuantityJLPT";
    private static final String KEY_FLASH_COST_JLPT = "flashCostJLPT";
    private static final String KEY_HIGHTEST_POINT_JLPT = "hightestPointJLPT";
    private static final String KEY_BUY_STATUS = "buyStatus";

    //Question table name
    private static final String TABLE_QUESTIONS = "questions";
    //Question Table Columns names
    private static final String KEY_ID_QUESTION = "idQuestion";
    private static final String KEY_ID_JLPT_FK = "idJLPT";
    private static final String KEY_QUESTION = "question";
    private static final String KEY_ANSWER_ONE = "answerOne";
    private static final String KEY_ANSWER_TWO = "answerTwo";
    private static final String KEY_ANSWER_THREE = "answerThree";
    private static final String KEY_ANSWER_FOUR = "answerFour";
    private static final String KEY_RIGHT_ANSWER = "rightAnswer";
    private static final String KEY_USER_ANSWER = "userAnswer";
    private static final String KEY_QUESTION_TYPE = "questionType";

    //Practice Question table
    private static final String TABLE_PRACTICE_QUESTIONS = "practiceQuestions";
    public UserDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_ARCHIVEMENT = "";
        String CREATE_JLPTS_TABLE = "CREATE TABLE " + TABLE_JLPTS + "("
                + KEY_ID_JLPT + " INTEGER PRIMARY KEY, "
                + KEY_TITLE_JLPT + " TEXT, "
                + KEY_TEST_STATUS + " INTEGER, "
                + KEY_BUY_STATUS + " INTEGER, "
                + KEY_FLASH_QUANTITY_JLPT + " INTEGER, "
                + KEY_FLASH_COST_JLPT + " INTEGER, "
                + KEY_HIGHTEST_POINT_JLPT + " FLOAT" + ");";
        db.execSQL(CREATE_JLPTS_TABLE);
        String INSERT_JLPTS_DATA = "INSERT INTO " + TABLE_JLPTS +"( titleJLPT,flashQuantityJLPT,flashCostJLPT,hightestPointJLPT,buyStatus) "
                + "VALUES (\"Moji - Goi Test 1\",2,5,0,1) ,"
                +"(\"Moji - Goi Test 2\",2,5,0,1) ,"
                +"(\"Moji - Goi Test 3\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 4\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 5\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 6\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 7\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 8\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 9\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 10\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 11\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 12\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 13\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 14\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 15\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 16\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 17\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 18\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 19\",2,5,0,0) ,"
                +"(\"Moji - Goi Test 20\",2,5,0,0);";
        db.execSQL(INSERT_JLPTS_DATA);
        String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + KEY_ID_QUESTION + " INTEGER PRIMARY KEY autoincrement, "
                + KEY_ID_JLPT_FK + " INTEGER, "
                + KEY_QUESTION +" TEXT, "
                + KEY_ANSWER_ONE +" TEXT, "
                + KEY_ANSWER_TWO + " TEXT, "
                + KEY_ANSWER_THREE + " TEXT, "
                + KEY_ANSWER_FOUR + " TEXT, "
                + KEY_RIGHT_ANSWER + " INTEGER, "
                + KEY_USER_ANSWER + " INTEGER, "
                + KEY_QUESTION_TYPE + " INTEGER, "
                + " FOREIGN KEY (" + KEY_ID_JLPT_FK + ") REFERENCES " + TABLE_JLPTS + "(" + KEY_ID_JLPT + "));";
        db.execSQL(CREATE_QUESTIONS_TABLE);


        String INSERT_QUESTION_DATA = "INSERT INTO " + TABLE_QUESTIONS + " (idJLPT,question,answerOne,answerTwo,answerThree,answerFour,rightAnswer,userAnswer,questionType) VALUES "

                + DatabaseHelper.QUESTION_DATA;
        db.execSQL(INSERT_QUESTION_DATA);
        String INSERT_QUESTION_DATA_0 = "INSERT INTO " + TABLE_QUESTIONS + " (idJLPT,question,answerOne,answerTwo,answerThree,answerFour,rightAnswer,userAnswer,questionType) VALUES "

                + DatabaseHelper.QUESTION_DATA_0;
        db.execSQL(INSERT_QUESTION_DATA_0);

        String CREATE_PRACTICE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_PRACTICE_QUESTIONS + "("
                + KEY_ID_QUESTION + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_QUESTION +" TEXT, "
                + KEY_ANSWER_ONE +" TEXT, "
                + KEY_ANSWER_TWO + " TEXT, "
                + KEY_ANSWER_THREE + " TEXT, "
                + KEY_ANSWER_FOUR + " TEXT, "
                + KEY_RIGHT_ANSWER + " INTEGER, "
                + KEY_USER_ANSWER + " INTEGER, "
                + KEY_QUESTION_TYPE + " INTEGER);";

        db.execSQL(CREATE_PRACTICE_QUESTIONS_TABLE);

        String INSERT_PRACTICE_QUESTIONS_DATA = "INSERT INTO " +  TABLE_PRACTICE_QUESTIONS+ "(question,answerOne,answerTwo,answerThree,answerFour,rightAnswer,userAnswer,questionType) VALUES"
                + DatabaseHelper.PRACTICE_QUESTIONS_DATA;
        db.execSQL(INSERT_PRACTICE_QUESTIONS_DATA);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JLPTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        // Create tables again
        onCreate(db);
    }

    //Get query ..........

    public List<JLPT> getListJLPT(int buy){
        List<JLPT> jlpts = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from jlpts", null );
        res.moveToFirst();
        while(res.isAfterLast() == false){

            if (res.getInt(3)==buy){
                jlpts.add(new JLPT(res.getInt(0),res.getString(1),res.getInt(2),res.getInt(3),res.getInt(4),res.getInt(5),res.getFloat(6)));
            }
            res.moveToNext();
        }
        return jlpts;
    }

    public List<Question> getListQuestion(int jlptCategory){
        List<Question> questions = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from questions where idJLPT = " + jlptCategory, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            questions.add(new Question(res.getInt(0),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getString(6),res.getInt(7),res.getInt(8)));
            res.moveToNext();
        }
        return questions;
    }
    public List<Question> getListPracticeQuestion(int questionType){
        List<Question> questions = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from practiceQuestions where questionType =" +questionType, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            questions.add(new Question(res.getInt(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getInt(6),res.getInt(7)));
            res.moveToNext();
        }
        res.close();
        return questions;
    }
    // Set query
    public void updateBoughtJLPTItem(int idJLPT){
        String UPDATE_ITEM = "UPDATE jlpts SET buyStatus = 1 WHERE idJLPT = "+idJLPT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor  =db.rawQuery(UPDATE_ITEM,null);
        cursor.moveToFirst();
        cursor.close();
    }
}
